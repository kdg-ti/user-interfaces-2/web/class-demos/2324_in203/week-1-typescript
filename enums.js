"use strict";
var Direction;
(function (Direction) {
    Direction["NORTH"] = "North";
    Direction["WEST"] = "West";
    Direction["SOUTH"] = "South";
    Direction["EAST"] = "East";
})(Direction || (Direction = {}));
let dir = Direction.NORTH;
console.log(dir);
dir = Direction.EAST;
console.log(dir);
console.log(Object.values(Direction));
