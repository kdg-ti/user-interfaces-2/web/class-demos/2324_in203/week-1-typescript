interface Dier {
    naam: string
}

interface Hond extends Dier {
    kleur?: string;
    blaf(): void;
}

interface Kat extends Dier {
    huiskat: boolean;
}
const samson: Hond = {
    naam: "Samson",
    blaf: () => {
        console.log("gertjeeeeeeuuh")
    }
}

console.log(samson);
console.log(samson.kleur);
samson.blaf();


let x = [0,1,null, samson, "abc", "def", "ghi", "jkl", "mno", "pqr", "stu", "vwx", "yz"]

function printId (id : number | string | Dier) {
    console.log(`Your ID is ${id}`)
}

printId(1);
printId("abc");
printId(samson);


type WindowStates = "open" | "closed" | "minimized";
let myWindow: WindowStates = "open";

type KatDog = Kat & Hond;
let katDog: KatDog = {
    naam: "KatDog",
    huiskat: false,
    blaf: () => {
        console.log("miaaawoef");
    }
}

console.log(katDog);

katDog.kleur = "zwart";
console.log(katDog)


function example() {
    let x: string | number | boolean;
    x = Math.random() < 0.5;
    console.log(x)
    console.log(typeof x);
//                     ^?

    if (Math.random() < 0.5) {
        x = "hello";
        console.log(x)
        console.log(typeof x);
//                         ^?
    } else {
        x = 100;
        console.log(x)
        console.log(typeof x);
//                         ^?
    }

    return x;
//         ^?
}

example();


// Variabele van het type "number"
let value: number = 42;
// Functie geeft waarde terug als type any
function returnValue(): any {
    return value;
}
// "any" variabele aanmaken en de functie aanroepen
let returnedValue: any = returnValue();

if (typeof returnedValue === "number") {
    console.log("De variabele is een getal");
}



// Type Assertion met 'as' syntax
let strValue = "kopr" as string;

console.log(strValue); // "Dit is een string"

// "Dit is een string" als type number?
let numValue = value as number;
console.log(numValue);
